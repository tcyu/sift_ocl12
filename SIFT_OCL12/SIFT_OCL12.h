#include "opencv/cv.h"
#include "opencv/highgui.h"


#include <vector>
#define _USE_MATH_DEFINES
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#include <assert.h>
#include <opencv2/nonfree/features2d.hpp>
#include <fstream>
#include <CL/cl.h>
#include "CLUtil.hpp"
#include <iostream>
using namespace appsdk;
using namespace std;
using namespace cv;

struct feature
{
	int pos[2];
	int scale[2];
	double abs_sigma;
	double orient;					// (-180,180]
	double desc[128];
};

class oclSIFT
{
public:
	cl_context GPU_context;
	cl_program program;
	cl_device_id device;
	cl_kernel kernel;
	cl_int errorNumber;
	cl_int err;
	cl_uint num;
	cl_command_queue commandQueue;
private:
	LARGE_INTEGER clk0, clk1, dclk1;
	LARGE_INTEGER freq;
	double	dt1;

	LARGE_INTEGER clk0_lm, clk1_lm, dclk1_lm;
	LARGE_INTEGER freq_lm;
	double	dt1_lm;

	cl_int err;
//	cl_command_queue queue;
protected:


public:
	float max27(float* arr);
	float min27(float* arr);
	Point3d subpixel(float* arr);
	Point2d suborientation(double* arr);
	Point2f interp(Point2d p, Mat& image);
	void constructDoG(Mat image, const int octaves, const int intervals, const double initial_sigma, vector< vector<Mat> >& imageblurs, vector< vector<double> >& absolute_sigma, vector< vector<Mat> >& DoG);
	int localmax(vector< vector<Mat> >& DoG, const  int octaves, const int intervals, const float contrast_threshold, vector<feature>& features, vector< vector<double> >& absolute_sigma);
	int major_orientation(vector<feature>& features, int octaves, int intervals, vector< vector<Mat> >& imageblurs, vector< vector<double> >& absolute_sigma);
	void descriptor(vector<feature>& features, int octaves, int intervals, vector< vector<Mat> >& imageblurs, vector< vector<double> >& absolute_sigma);
	void mySIFT(Mat image, int octaves, int intervals, double initial_sigma, float contrast_threshold, vector<feature>& features);
	void FeatureDetection(Mat &image, Mat &descriptor, vector<KeyPoint> &keypoints);
	int setupCL();
	oclSIFT()
	{
		
	};

	~oclSIFT()
	{

	};

};
